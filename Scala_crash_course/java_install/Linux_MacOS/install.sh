
#Check your version
J.T. Kirk:~$ java -version
java version "1.7.0_71"
Java(TM) SE Runtime Environment (build 1.7.0_71-b14)
Java HotSpot(TM) 64-Bit Server VM (build 24.71-b01, mixed mode) 

#Purge openJDK
sudo apt-get purge openjdk-\*

#Prepare the place for Java
sudo mkdir -p /usr/local/java

#copy and unzip the archive into /usr/local/java
cd /home/"your_user_name"/Downloads
sudo cp -r jdk<YOUR DOWNLOADED ARCHIVE>.tar.gz /usr/local/java/
cd /usr/local/java
sudo tar xvzf *.tar.gz
ls -a #to check : you must see two directories : a "jdk" and a "jre".

#Edit /etc/profile
sudo (vim, emacs, or anoteher editor) /etc/profile
JAVA_HOME=/usr/local/java/<your jdk dirname>
PATH=$PATH:$HOME/bin:$JAVA_HOME/bin
export JAVA_HOME
export PATH

#Inform your system about the Java installation dir
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/local/java/<your jdk dir name>/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/local/java/<your jdk dirname>/bin/javac" 1
sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/local/java/<your jdk dirname>/bin/javaws" 1

#Inform your system about the Java version to use
sudo update-alternatives --set java /usr/local/java/<your jdk dir name>/bin/java
sudo update-alternatives --set javac /usr/local/java/<your jdk dir name>/bin/javac
sudo update-alternatives --set javaws /usr/local/java/<your jdk dir name>/bin/javaws

#Reload the profile file
source /etc/profile

#And reboot your system. After reboot run
java -version
javac -version
